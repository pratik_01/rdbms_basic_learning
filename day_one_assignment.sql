drop table if exists airplanes_seats;
drop table if exists airplanes;

-- Below DDL commands creates airplanes table.

create table if not exists airplanes(
  id serial primary key,
  name varchar(25),
  total_number_of_seats integer,
  number_of_business_class_seats integer,
  number_of_business_class_cols integer,
  number_of_business_class_rows integer,
  total_number_of_usable_rows integer,
  number_of_economy_class_cols integer
);


-- Below DML commands are used to insert data into the airplanes table.
insert into airplanes(name, total_number_of_seats, number_of_business_class_seats, number_of_business_class_cols, total_number_of_usable_rows, number_of_business_class_rows,
number_of_economy_class_cols) values('airplane_a', 70, 6, 3, 18, 2, 4);
insert into airplanes(name, total_number_of_seats, number_of_business_class_seats, number_of_business_class_cols, total_number_of_usable_rows, number_of_business_class_rows,
number_of_economy_class_cols) values('airplane_b', 70, 16, 4, 22, 4, 6);

-- Below table is dependent upon the airplanes table.

create table if not exists airplanes_seats(
  id serial primary key,
  airplane_name_id integer,
  row_number integer,
  column_number integer,
  class_type varchar(25),
  seat_type varchar(25),
  foreign key(airplane_name_id)
  references airplanes(id)
);


-- Below DML commands are used to insert data into the airplanes_seats table.
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(1, 2, 2, 'Business', 'Aisle');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(1, 8, 2, 'Economy Plus', 'Aisle');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(1, 10, 4, 'Economy Plus', 'Window');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(2, 1, 1, 'Business', 'Window');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(2, 4, 2, 'Business', 'Window');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(2, 4, 4, 'Business', 'Window');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(2, 11, 3, 'Economy', 'Aisle');
insert into airplanes_seats(airplane_name_id, row_number, column_number, class_type, seat_type) values(2, 21, 6, 'Business', 'Window');


--Below DQL commands are used to fetch data from the tables.

select name, (total_number_of_seats - number_of_business_class_seats) as total_number_of_economy_seats from airplanes;
select name, (total_number_of_seats - number_of_business_class_rows)/(2) as number_of_aisle_seats from airplanes;
select name, (total_number_of_usable_rows)*2 as number_of_window_seats from airplanes;
select CASE WHEN number_of_business_class_cols <= 4 AND number_of_economy_class_cols <= 4 THEN total_number_of_usable_rows WHEN number_of_economy_class_cols >= 4 AND number_of_business_class_cols >= 4 THEN 0 END rows_without_middle_seat from airplanes;
select (plane.total_number_of_usable_rows - count(distinct seats.row_number)) as completely_empty_row, plane.name from airplanes_seats as seats JOIN airplanes as plane on seats.airplane_name_id = plane.id where seats.row_number in (select * from generate_series(0, (plane.total_number_of_usable_rows))) GROUP BY plane.name, plane.total_number_of_usable_rows;
select (plane.total_number_of_seats - count(seats.id)) as num_of_empty_seats, plane.name from airplanes_seats as seats join airplanes as plane on seats.airplane_name_id = plane.id group by plane.name, plane.total_number_of_seats;
select (count(seats.id)) as num_of_occupied_seats, plane.name from airplanes_seats as seats join airplanes as plane on seats.airplane_name_id = plane.id group by plane.name, plane.total_number_of_seats;


