drop table if exists seats;
drop table if exists rows;
drop table if exists airplane;


CREATE TABLE airplane(

  id serial primary key,
  name varchar,
  number_of_rows int
);

CREATE TABLE if not exists rows(
  id serial primary key,
  class_type varchar,
  row_number int,
  plane_id int,
  foreign key(plane_id)
  references airplane(id)
);

CREATE TABLE if not exists seats(
  id serial primary key,
  row_id int,
  seat_type varchar,
  seat_number int,
  is_occupied boolean,
  is_active boolean,
  foreign key(row_id)
  references rows(id)
);


INSERT INTO airplane(name, number_of_rows) values('airplane_a', 18);
INSERT INTO airplane(name, number_of_rows) values('airplane_b', 25);
INSERT INTO airplane(name, number_of_rows) values('airplane_c', 60);

INSERT INTO rows(class_type, row_number, plane_id) values('United First', 1, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('United First', 2, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy Plus', 7, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy Plus', 8, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy Plus', 10, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy', 11, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy', 12, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy', 16, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy', 20, 1);
INSERT INTO rows(class_type, row_number, plane_id) values('Economy', 24, 1);